import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# Loading the dataset
data = pd.read_csv('Dataset Churn prediction 04-08 - data60.csv')
data.head()
data.shape
# Defining custom bins based on the provided intervals
bins = [-np.inf, 0, 3, 4, 10, 40, np.inf]
labels = ['0', '1-3', '1-4', '5-10', '20-40', '>40']

# Binning the 'leads' data based on the custom bins
data['leads_custom_binned'] = pd.cut(data['leads'], bins=bins, labels=labels, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_custom_bins = data.groupby(['leads_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_custom_bins = grouped_custom_bins.pivot(index='leads_custom_binned', columns='has_churned', values='counts')
pivot_grouped_custom_bins.fillna(0, inplace=True)

# Calculating percentages for visualization
total_counts_custom_bins = pivot_grouped_custom_bins.sum(axis=1)
pivot_grouped_custom_bins_percentage = (pivot_grouped_custom_bins.divide(total_counts_custom_bins, axis=0) * 100)

colors = ['lightblue', 'salmon']

# Plotting
ax = pivot_grouped_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Leads')
plt.ylabel('Percentage')
plt.title('Leads (Custom Bins) vs Churn')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
# Plotting the actual numbers
colors = ['lightblue', 'salmon']
ax = pivot_grouped_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Leads')
plt.ylabel('Count')
plt.title('Leads (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
#### % Hired ####
# Defining custom bins for '% Hired' based on the provided intervals
bins_hired = [-np.inf, 0, 1, 4, 10, np.inf]
labels_hired = ['0', '1', '2-4', '5-10', '>10']

# Binning the '% Hired' data based on the custom bins
data['hired_custom_binned'] = pd.cut(data['% Hired'], bins=bins_hired, labels=labels_hired, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_hired_custom_bins = data.groupby(['hired_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_hired_custom_bins = grouped_hired_custom_bins.pivot(index='hired_custom_binned', columns='has_churned', values='counts')
pivot_grouped_hired_custom_bins.fillna(0, inplace=True)

# Plotting the actual numbers with specified colors
ax = pivot_grouped_hired_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('% Hired')
plt.ylabel('Count')
plt.title('% Hired (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
# Calculating percentages for visualization
total_counts_hired_custom_bins = pivot_grouped_hired_custom_bins.sum(axis=1)
pivot_grouped_hired_custom_bins_percentage = (pivot_grouped_hired_custom_bins.divide(total_counts_hired_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_hired_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('% Hired')
plt.ylabel('Percentage')
plt.title('% Hired (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
#### Answered by Email ####
# Defining custom bins for 'answered_by_email' based on the provided intervals
bins_email = [-np.inf, 0, 20, 50, 75, np.inf]
labels_email = ['0%', '1-20%', '21-50%', '51-75%', '>75%']

# Binning the 'answered_by_email' data based on the custom bins
data['email_custom_binned'] = pd.cut(data['answered_by_email'], bins=bins_email, labels=labels_email, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_email_custom_bins = data.groupby(['email_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_email_custom_bins = grouped_email_custom_bins.pivot(index='email_custom_binned', columns='has_churned', values='counts')
pivot_grouped_email_custom_bins.fillna(0, inplace=True)

# Calculating percentages for visualization
total_counts_email_custom_bins = pivot_grouped_email_custom_bins.sum(axis=1)
pivot_grouped_email_custom_bins_percentage = (pivot_grouped_email_custom_bins.divide(total_counts_email_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_email_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Answered by Email')
plt.ylabel('Percentage')
plt.title('Answered by Email (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
# Plotting the actual numbers for 'Answered by Email' with specified colors
ax = pivot_grouped_email_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Answered by Email')
plt.ylabel('Count')
plt.title('Answered by Email (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
#### Answered by phone ####
# Defining custom bins for 'answered_by_phone' based on the provided intervals
bins_phone = [-np.inf, 0, 20, 50, 75, np.inf]
labels_phone = ['0%', '1-20%', '21-50%', '51-75%', '>75%']

# Binning the 'answered_by_phone' data based on the custom bins
data['phone_custom_binned'] = pd.cut(data['answered_by_phone'], bins=bins_phone, labels=labels_phone, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_phone_custom_bins = data.groupby(['phone_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_phone_custom_bins = grouped_phone_custom_bins.pivot(index='phone_custom_binned', columns='has_churned', values='counts')
pivot_grouped_phone_custom_bins.fillna(0, inplace=True)

# Plotting the actual numbers for 'Answered by Phone' with specified colors
ax = pivot_grouped_phone_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Answered by Phone')
plt.ylabel('Count')
plt.title('Answered by Phone (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
# Calculating percentages for visualization for 'Answered by Phone'
total_counts_phone_custom_bins = pivot_grouped_phone_custom_bins.sum(axis=1)
pivot_grouped_phone_custom_bins_percentage = (pivot_grouped_phone_custom_bins.divide(total_counts_phone_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_phone_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Answered by Phone')
plt.ylabel('Percentage')
plt.title('Answered by Phone (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
#### Reclaimed Percentage ####
# Defining custom bins for 'reclaimed_percent' based on the provided intervals
bins_reclaimed = [-np.inf, 0, 10, 20, 30, 40, np.inf]
labels_reclaimed = ['0%', '1-10%', '11-20%', '21-30%', '31-40%', '>40%']

# Binning the 'reclaimed_percent' data based on the custom bins
data['reclaimed_custom_binned'] = pd.cut(data['reclaimed_percent'], bins=bins_reclaimed, labels=labels_reclaimed, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_reclaimed_custom_bins = data.groupby(['reclaimed_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_reclaimed_custom_bins = grouped_reclaimed_custom_bins.pivot(index='reclaimed_custom_binned', columns='has_churned', values='counts')
pivot_grouped_reclaimed_custom_bins.fillna(0, inplace=True)

# Calculating percentages for visualization
total_counts_reclaimed_custom_bins = pivot_grouped_reclaimed_custom_bins.sum(axis=1)
pivot_grouped_reclaimed_custom_bins_percentage = (pivot_grouped_reclaimed_custom_bins.divide(total_counts_reclaimed_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_reclaimed_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Reclaimed Percent')
plt.ylabel('Percentage')
plt.title('Reclaimed Percent (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
# Plotting the actual numbers for 'Reclaimed Percent' with specified colors
ax = pivot_grouped_reclaimed_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Reclaimed Percent')
plt.ylabel('Count')
plt.title('Reclaimed Percent (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
#### Reclaimed Denied Percent ####
# Defining custom bins for 'reclaimed_denied_percent' based on the provided intervals
bins_denied = [-np.inf, 0, 5, 10, 20, np.inf]
labels_denied = ['0%', '1-5%', '6-10%', '11-20%', '>20%']

# Binning the 'reclaimed_denied_percent' data based on the custom bins
data['denied_custom_binned'] = pd.cut(data['reclaimed_denied_percent'], bins=bins_denied, labels=labels_denied, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_denied_custom_bins = data.groupby(['denied_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_denied_custom_bins = grouped_denied_custom_bins.pivot(index='denied_custom_binned', columns='has_churned', values='counts')
pivot_grouped_denied_custom_bins.fillna(0, inplace=True)

# Plotting the actual numbers for 'Reclaimed Denied Percent' with specified colors
ax = pivot_grouped_denied_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Reclaimed Denied Percent')
plt.ylabel('Count')
plt.title('Reclaimed Denied Percent (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()


# Calculating percentages for visualization for 'Reclaimed Denied Percent'
total_counts_denied_custom_bins = pivot_grouped_denied_custom_bins.sum(axis=1)
pivot_grouped_denied_custom_bins_percentage = (pivot_grouped_denied_custom_bins.divide(total_counts_denied_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_denied_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Reclaimed Denied Percent')
plt.ylabel('Percentage')
plt.title('Reclaimed Denied Percent (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
#### Total Score ####

# Defining custom bins for 'total_score' based on the provided intervals
bins_score = [7, 8, 8.5, 9, np.inf]
labels_score = ['7-8', '8-8.5', '8.5-9', '>9']

# Binning the 'total_score' data based on the custom bins
data['score_custom_binned'] = pd.cut(data['total_score'], bins=bins_score, labels=labels_score, include_lowest=True, right=True)

# Grouping by the custom bins and 'has_churned' column
grouped_score_custom_bins = data.groupby(['score_custom_binned', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_score_custom_bins = grouped_score_custom_bins.pivot(index='score_custom_binned', columns='has_churned', values='counts')
pivot_grouped_score_custom_bins.fillna(0, inplace=True)

# Calculating percentages for visualization
total_counts_score_custom_bins = pivot_grouped_score_custom_bins.sum(axis=1)
pivot_grouped_score_custom_bins_percentage = (pivot_grouped_score_custom_bins.divide(total_counts_score_custom_bins, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_score_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Total Score')
plt.ylabel('Percentage')
plt.title('Total Score (Custom Bins) vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()
# Plotting the actual numbers for 'Total Score' with specified colors
ax = pivot_grouped_score_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Total Score')
plt.ylabel('Count')
plt.title('Total Score (Custom Bins) vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
#### Widget active ####
# Grouping by the 'widget_active' and 'has_churned' column
grouped_widget_active = data.groupby(['widget_active', 'has_churned']).size().reset_index(name='counts')

# Creating a pivot table for visualization
pivot_grouped_widget_active = grouped_widget_active.pivot(index='widget_active', columns='has_churned', values='counts')
pivot_grouped_widget_active.fillna(0, inplace=True)

# Calculating percentages for visualization
total_counts_widget_active = pivot_grouped_widget_active.sum(axis=1)
pivot_grouped_widget_active_percentage = (pivot_grouped_widget_active.divide(total_counts_widget_active, axis=0) * 100)

# Plotting the percentages with specified colors
ax = pivot_grouped_widget_active_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Widget Active')
plt.ylabel('Percentage')
plt.title('Widget Active vs Churn - Percentage')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{height:.2f}%', (x + width/2, y + height + 0.5), ha='center')

plt.show()

# Plotting the actual numbers for 'Widget Active' with specified colors
ax = pivot_grouped_widget_active.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Widget Active')
plt.ylabel('Count')
plt.title('Widget Active vs Churn - Actual Numbers')

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')

plt.show()
#### Response Time ####
import numpy as np
import matplotlib.pyplot as plt
# Defining custom bins for 'response_time' based on the provided intervals
bins_response_time = [-np.inf, 6, 12, 24, 48, np.inf]
labels_response_time = ['0-6hrs', '7-12hrs', '13-24hrs', '25-48hrs', '>48hrs']
# Binning the 'response_time' data based on the custom bins
data['response_time_custom_binned'] = pd.cut(data['response_time'], bins=bins_response_time, labels=labels_response_time, include_lowest=True, right=True)
# Grouping by the custom bins and 'has_churned' column
grouped_response_time_custom_bins = data.groupby(['response_time_custom_binned', 'has_churned']).size().reset_index(name='counts')
# Creating a pivot table for visualization
pivot_grouped_response_time_custom_bins = grouped_response_time_custom_bins.pivot(index='response_time_custom_binned', columns='has_churned', values='counts')
pivot_grouped_response_time_custom_bins.fillna(0, inplace=True)
# Plotting the actual numbers for 'Response Time' with specified colors
colors = ['lightblue', 'salmon']
ax = pivot_grouped_response_time_custom_bins.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Response Time')
plt.ylabel('Count')
plt.title('Response Time (Custom Bins) vs Churn - Actual Numbers')
for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax.annotate(f'{int(height)}', (x + width/2, y + height + 5), ha='center')
plt.show()
# Calculating the percentages for each bin and churn status
pivot_grouped_response_time_custom_bins_percentage = pivot_grouped_response_time_custom_bins.divide(pivot_grouped_response_time_custom_bins.sum(axis=1), axis=0) * 100
# Plotting the percentages for 'Response Time' with specified colors
ax_percentage = pivot_grouped_response_time_custom_bins_percentage.plot(kind='bar', stacked=False, figsize=(12,7), color=colors)
plt.xlabel('Response Time')
plt.ylabel('Percentage (%)')
plt.title('Response Time (Custom Bins) vs Churn - Percentage')
for p in ax_percentage.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy()
    ax_percentage.annotate(f'{height:.2f}%', (x + width/2, y + height + 1), ha='center')
plt.show()




